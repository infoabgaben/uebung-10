package bestellung;

public class PremiumKunde extends Kunde{

	private float discount;
	
	 public PremiumKunde(String name, String address, float discount) {
		super(name, address);
		this.discount = discount;
	}
	
	@Override
	public float berechneDiscount() {
		return discount;
	}

}
