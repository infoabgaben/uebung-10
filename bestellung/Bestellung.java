package bestellung;

import java.util.Date;

public class Bestellung {

	private Date bestellDatum;
	private Kunde kunde;
	
	private ProduktStatus[] items;
	
	public Bestellung(Kunde kunde) {
		this(new Date(), kunde);
	}
	
	public Bestellung(Date date, Kunde kunde) {
		bestellDatum = date;
		this.kunde = kunde;
		items = new ProduktStatus[100];
	}

	public float nettoPreis() {
		float result = 0;
		for(ProduktStatus ps : items) {
			if(ps != null)
				result += ps.gesamtPreis();
		}
		return result * (1 - kunde.berechneDiscount());
	}
	
	public void produktHinzufuegen(Produkt produkt, int anzahl) {
		ProduktStatus ps = new ProduktStatus(produkt, anzahl);
		for(int i = 0; i < items.length; i++) {
			if(items[i] == null) {
				items[i] = ps;
				return;
			}
		}
	}
}
