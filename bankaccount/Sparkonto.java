package bankaccount;

public class Sparkonto extends Bankkonto{

	private float zins;
	
	public Sparkonto(int kontonummer, float guthaben, float zins) {
		super(kontonummer, guthaben);
		this.zins = zins;
	}
	
	public Sparkonto(int kontonummer, float zins) {
		super(kontonummer);
		this.zins = zins;
	}

	public void zinsAuszahlung() {
		float zinsen = this.getGuthaben() * zins / 12;
		this.einzahlen(zinsen);
	}
	
}
