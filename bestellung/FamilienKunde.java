package bestellung;

public class FamilienKunde extends Kunde{

	private Kunde[] familienMitglieder;
	
	public FamilienKunde(String name, String address) {
		super(name, address);
		familienMitglieder = new Kunde[4];
	}
	
	public void familienMitgliedHinzufuegen(Kunde kunde) {
		if(kunde != this) {
			for(int i = 0; i < familienMitglieder.length; i++) {
				if(familienMitglieder[i] == null) {
					familienMitglieder[i] = kunde;
					return;
				}
					
			}				
		}
	}

	@Override
	public float berechneDiscount() {
		int members = 0;
		for(Kunde kunde : familienMitglieder) {
			if(kunde != null)
				members++;
		}
		return members * 0.05f;
	}

}
