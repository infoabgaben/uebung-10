package bestellung;

import java.util.Date;

public class TesteBestellung {	
	public static void main(String[] args) {
		// erzeuge kunden
		PremiumKunde  kunde1 = new PremiumKunde("Peter Parker", "20 Ingram Street, New York", 0.15f);
		PremiumKunde  kunde2 = new PremiumKunde("Ellen Ripley", "USCSS Nostromo", 0.22f);
		PremiumKunde  kunde3 = new PremiumKunde("Sherlock Holmes", "221b Baker St, London NW16XE", 0.12f);
		PremiumKunde  kunde4 = new PremiumKunde("Daenerys Targaryen", "Castle Dragonstone", 0.18f);
		FamilienKunde kunde5 = new FamilienKunde("Obi-Wan Kenobi", "Tatooine");
		
		// kunden zur familie hinzufuegen
		kunde5.familienMitgliedHinzufuegen(kunde1);
		kunde5.familienMitgliedHinzufuegen(kunde2);
		kunde5.familienMitgliedHinzufuegen(kunde3);
		kunde5.familienMitgliedHinzufuegen(kunde4);
		
		// erzeuge einige produkte
		Produkt produkt1 = new Produkt("Netzschiesser", 340.90f);
		Produkt produkt2 = new Produkt("Violine", 14900.00f);
		Produkt produkt3 = new Produkt("Drachenglas", 200.00f);
		Produkt produkt4 = new Produkt("Lichtschwert", 20000.00f);
		
		// einkauf kunde 1
		Bestellung bestellung1 = new Bestellung(new Date(), kunde1);
		bestellung1.produktHinzufuegen(produkt2, 3);
		bestellung1.produktHinzufuegen(produkt4, 1);
		System.out.println("Kunde 1 Nettopreis: " + bestellung1.nettoPreis());
		assert(Math.abs(bestellung1.nettoPreis() - 54995) < 1e-3);
		
		// einkauf kunde 5
		Bestellung bestellung2 = new Bestellung(new Date(), kunde5);
		bestellung2.produktHinzufuegen(produkt1, 12);
		bestellung2.produktHinzufuegen(produkt3, 20);
		System.out.println("Kunde 5 Nettopreis: " + bestellung2.nettoPreis());
		assert(Math.abs(bestellung1.nettoPreis() - 54995) < 1e-3);
	}
}
