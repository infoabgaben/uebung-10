package bestellung;

public class ProduktStatus {

	private Produkt produkt;
	private int anzahl;
	
	public ProduktStatus(Produkt produkt, int anzahl) {
		this.produkt = produkt;
		this.anzahl = anzahl;
	}
	
	public float gesamtPreis() {
		return produkt.getPreis() * anzahl;
	}
	
	public Produkt getProdukt() {
		return produkt;
	}
	
	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}
	
	public int getAnzahl() {
		return anzahl;
	}
	
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	

	
}
