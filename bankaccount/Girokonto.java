package bankaccount;

public class Girokonto extends Bankkonto {

	private int transactions;
	
	private final int FREE_TRANSACTIONS = 5;
	private final float COSTS_PER_TRANSACTION = 0.50f; 
	private final float DISPO = 50f;
			
	public Girokonto(int kontonummer, float guthaben) {
		super(kontonummer, guthaben);
		transactions = 0;
	}
	
	public Girokonto(int kontonummer) {
		this(kontonummer, 0.0f);
	}

	@Override
	public void einzahlen(float betrag) {
		super.einzahlen(betrag);
		transactions++;
	}
	
	@Override
	public void abheben(float betrag) {
		if(super.getGuthaben() - betrag > -1 * DISPO)
			super.abheben(betrag);
		else
			super.abheben(0.5f);
		transactions++;
	}
	
	public void monatsEnde() {
		int t = transactions - FREE_TRANSACTIONS; 
		if(t > 0)
			super.abheben(t * COSTS_PER_TRANSACTION);
		transactions = 0;
	}
}
