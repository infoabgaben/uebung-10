package bankaccount;

public class Bankkonto {
 
	private int id;
	private float balance;
	
	public Bankkonto(int kontonummer, float guthaben) {
		id = kontonummer;
		balance = guthaben;
	}
	
	public Bankkonto(int kontonummer) {
		this(kontonummer, 0.0f);
	}
	
	public void einzahlen(float betrag) {
		if(betrag < 0)
			System.err.println("Es k�nnen nur positive Betr�ge eingezahlt werden!");
		else
			balance += betrag;
	}
 	
	public void abheben(float betrag) {
		if(betrag < 0)
			System.err.println("Es k�nnen nur positive Betr�ge abgehoben werden!");
		else
			balance -= betrag;
	}
 	
 	public float getGuthaben() {
		return balance;
	}
 	
	public int getID() {
		return id;
	}

	public String toString() {
		 return "Konto: " + id + ": " + "Guthaben = " + balance;    
	}

	public final void print() {
		System.out.println(toString());    
	}
}